# Truth ins’t truth

Projet WebGL réalisé dans le cadre de la fomration "Design & Management de l'innovation interactive" à l'école des Gobelins (Annecy).


### Demo :

Pour accéder au projet directement en ligne, suivez le lien ci-contre : http://webgl.theopazzaglia.fr/ 


### Fonctionnalités :

- [x] Intégration des modèles 3D.
- [x] Création des modèles de données.
- [x] Gestion des différentes vues de la caméra.
- [x] Transition et animations des différentes scènes.
- [x] Transition et animations des différentes bâtiments constituant les scènes.
- [x] Clic (Raycaster) sur les différentes scènes et points.
- [x] Effets / Shader sur les différents boutons (points). 


### ToDo:

Voir le Trello : https://trello.com/b/5uC6jaIZ/webgl-dev

- [] Intégration des maquettes Desktop à finir
- [] Intégration des maquettes Mobile à finir


### Exigences :

- Node.js : https://nodejs.org/en/


### Installation :

1. Clonez ce projet dans le dossier de votre choix : https://bitbucket.org/piwiid/webgl_groupe_a/src/master/

2. Depuis votre dossier, accédez au projet et installez les dépendances : `npm install`

3. Pour lancez le serveur en mode développement et accédez à l'application, lancez la commande suivante : `npm run dev`

4. Pour build la version de production de l'application, lancez la commande suivante : `npm run build`

5. Si vous ne possèdez pas Node.js, vous pouvez utiliser le dossier `/dist` avec un serveur (exemple MAMP).


### Architecture : 

- `src` : dossier principale de l'application?
	- `ressources` : contient les fichiers ressources de l'application (images, styles, models3D, libs).
	- `scripts` : content les dossiers applicatifs et de configurations
		- `configs` : dossier configuration
		- `app` : dossier applicatif
			- `components`
			- `controllers`
			- `datasources` : regroupe les données
			- `helpers` : regroupe les scripts utilitaires
			- `managers` : regroupe les scripts utilitaires
			- `main.js` : script d'initialisation 
		- `index.js` : fichier d'initialisation