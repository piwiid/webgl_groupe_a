const Path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ThreeWebpackPlugin = require('@wildpeaks/three-webpack-plugin');


const dest = Path.join(__dirname, '../dist');

module.exports = {
  entry: [
    Path.resolve(__dirname, './polyfills'),
    Path.resolve(__dirname, '../src/scripts/index')
  ],
  output: {
    path: dest,
    filename: 'bundle.[hash].js'
  },
  plugins: [
    new ThreeWebpackPlugin(),
    new CleanWebpackPlugin([dest], { root: Path.resolve(__dirname, '..') }),
    new CopyWebpackPlugin([
      { from: Path.resolve(__dirname, '../public'), to: 'public' }
    ]),
    new HtmlWebpackPlugin({
      template: Path.resolve(__dirname, '../src/index.html')
    })
  ],
  resolve: {
    alias: {
      '~': Path.resolve(__dirname, '../src'),
      'ressources': Path.resolve(__dirname, '../src/ressources'),
      'main': Path.resolve(__dirname, '../src/scripts/app/Main.js'),
      'configs': Path.resolve(__dirname, '../src/scripts/configs'),
      'components': Path.resolve(__dirname, '../src/scripts/app/components'),    
      'controllers': Path.resolve(__dirname, '../src/scripts/app/controllers'),
      'datasources': Path.resolve(__dirname, '../src/scripts/app/datasources'),
      'helpers': Path.resolve(__dirname, '../src/scripts/app/helpers'),
      'managers': Path.resolve(__dirname, '../src/scripts/app/managers'),
    },
    extensions: ['.js', '.jsx']
  },
  devServer: {
      headers: {
          'Access-Control-Allow-Origin': '*'
      }
  },
  module: {
    rules: [

      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|dae|obj|mtl)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }
      }
    ]
  }
};
