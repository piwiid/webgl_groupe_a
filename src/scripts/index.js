/**
*
* Index.js
*
**/

// Utils dependencies
import WebDetector from 'helpers/utils/WebDetector';

// Managers & Main class
import UIManager from 'managers/UIManager';
//import Main from 'main';

// Import Tween
import SplitText from 'ressources/libs/SplitText.min'
import {TimelineLite } from 'gsap';
import StartAnimation from "./app/helpers/utils/StartAnimation";

export default class Index {

	init() {
		// Check for webGL capabilities
    	if(!WebDetector.webgl) {
        	WebDetector.addGetWebGLMessage();
    	} else {
            this.UIManager  = new UIManager();
    		this.UIManager.setupTitleScreen();
    		this.UIManager.setupContainer();
        }
	}
}

// Index instructions
let index = new Index();
index.init();
