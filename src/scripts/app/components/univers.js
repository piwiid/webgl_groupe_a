/**
*
* Univers.js
*
**/

// Global dependencies
import * as THREE from 'three';
import TWEEN from 'tween.js';

// Config dependencies
import Config from 'configs/Config';


export default class Univers {
    constructor(scene, camera) {

        this.scene = scene;

        this.camera = camera.threeCamera;


        this.objects = [];

        let group = new THREE.Group();

        var sphere, sphere2;

        this.mouse = new THREE.Vector2();


        var light = new THREE.DirectionalLight( 0xffffff, 1 );
        light.position.set( 1, 1, 1 ).normalize();
        scene.add( light );

        scene.add(group);


        var geometry = new THREE.SphereGeometry( 50, 10, 10 );
        var material = new THREE.MeshLambertMaterial( {color: 0xffff00} );
        sphere = new THREE.Mesh( geometry, material );
        group.add(sphere)
        var sphere2 = new THREE.Mesh( geometry, material );
        scene.add(sphere2);

        sphere.position.x = -200
        sphere2.position.x = 200

        this.objects.push(sphere)
        this.objects.push(sphere2)


        // Listenets
        document.addEventListener('mousedown', this.onDocmentMouseDown.bind(this), false);
        document.addEventListener('touchstart', this.onDocumentTouchStart.bind(this), false);

        // Listeners
        //window.addEventListener('resize', () => this.updateSize(renderer), false);
    }

    onDocumentMouseDown(event) {

        event.preventDefault();

        this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;


        let raycaster = new THREE.Raycaster();


        raycaster.setFromCamera(this.mouse, this.camera);

        var intersects = raycaster.intersectObjects(this.objects, true);




        if(intersects.length > 0) {

            var altitude = 300;
            var rad = this.objects[0].position.distanceTo( intersects[ 0 ].point );
            var coeff = 1+ altitude/rad;

            //intersects[ 0 ].object.material.color.set( new THREE.Color(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`) );

            //setupTween (camera.position.clone(), new THREE.Vector3 (moveX,moveY,moveZ), 800);



            this.setupTween (this.camera.position.clone(), new THREE.Vector3 (intersects[ 0 ].point.x * coeff, intersects[ 0 ].point.y * coeff, intersects[ 0 ].point.z * coeff), 800);
            //testBool = false;
            //controls.enabled = false;

            //controls.enabled = false;

            /*camera.lookAt(new THREE.Vector3(0, 0, 0));
            camera.position.z = intersects[0].point.z * .9;
            camera.position.x = intersects[0].point.x * .9;
            camera.position.y = intersects[0].point.y * .9;*/


        }

    }

    onDocumentTouchStart(event) {

        event.preventDefault();
        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;


        document.addEventListener( 'mousemove', onDocumentMouseMove, false );
        document.addEventListener( 'mouseup', onDocumentMouseUp, false );
        document.addEventListener( 'mouseout', onDocumentMouseOut, false );

        mouseXOnMouseDown = event.clientX - windowHalfX;
        targetRotationOnMouseDownX = targetRotationX;

        mouseYOnMouseDown = event.clientY - windowHalfY;
        targetRotationOnMouseDownY = targetRotationY;

        this.onDocmentMouseDown(event,index);

    }
    setupTween (position, target, duration) {


        TWEEN.removeAll();    // remove previous tweens if needed

        new TWEEN.Tween (position)
            .to (target, duration)
            .easing (TWEEN.Easing.Linear.None)

            .onUpdate (
                () => {
                    this.camera.position.copy(position);

                })
            .onComplete(
                function() {
                    //testBool = true;
                }
            )

            .start();
    }

}
