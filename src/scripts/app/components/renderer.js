/**
*
* Renderer.js
* Main webGL renderer class
*
**/

// Global dependencies
import * as THREE from 'three';

// Config dependencies
import Config from 'configs/Config';


export default class Renderer {
  constructor(container) {
    // Properties
      this.container = container;

      this.threeRenderer = new THREE.WebGLRenderer({ alpha: true });


      this.threeRenderer.shadowMap.enabled = true;
      this.threeRenderer.setSize( window.innerWidth, window.innerHeight );
      this.threeRenderer.shadowMapType = THREE.PCFSoftShadowMap; // softer shadows
      this.threeRenderer.setPixelRatio((window.devicePixelRatio) ? window.devicePixelRatio : 1);

      this.container.appendChild(this.threeRenderer.domElement);
  }
}
