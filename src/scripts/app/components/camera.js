/**
*
* Camera.js
* Class that creates and updates the main camera
*
**/

// Global dependencies
import * as THREE from 'three';
import TWEEN from 'tween.js';
import { TweenMax } from 'gsap';
import Draggable from 'gsap/Draggable';

// Config dependencies
import Config from 'configs/Config';
import {Bus} from "../helpers/events/Bus";


export default class Camera {
  constructor(renderer) {

    this.renderer = renderer;
    const width = window.innerWidth;
    const height = window.innerHeight;

    this.cameraLookAt = null;
    this.controls = null;

    this.quaternion = new THREE.Quaternion;

    this.returnCamera = null;

    // Create and position a Perspective Camera
    this.threeCamera = new THREE.PerspectiveCamera(Config.camera.fov, width / height, Config.camera.near, Config.camera.far);
    this.handleCameraPosition(width);

    this.threeCamera.position.set(-232.15169811828193,368.55975766436114,-300.03388733631255);

    this.threeCamera.updateProjectionMatrix();

    this.last = false;
    this.down = false;
    this.delta = null;
    this.backToWorld = true;
    const positionCameraOnQuaternion = null;
    this.axis = new THREE.Vector3( 0, 1, 0 );


    this.isFirstMove = false;
    this.globalScene = true;
    this.cameraIsAnimating = false;
    this.tl = new TimelineLite;
    this.currentOnIsland = true;
    this.Bus = new Bus()

    this.friction = 0.9;
    this.mouseInteria = 0.9;
    this.clicking = true;

    this.initEventMouse();



      // Resize Listeners
    window.addEventListener('resize', () => this.onWindowResize(), false);
  }

    displayButtonExitIsland() {

        this.tl.fromTo('.panel-exit',1,{display:"none",autoAlpha:0},
            {
                autoAlpha:1,
                display:"flex",
                onComplete:() => {
                    document.querySelector('.panel-exit').addEventListener('click', () => {
                        if(!this.clicking) return
                        this.hideButtonExitIsland();
                        this.clicking = false;
                    })
                }
            });

    }

    hideButtonExitIsland() {


        this.goBackToQuartenion();
        this.tl.fromTo('.panel-exit',1,{display:"flex",autoAlpha:1},
            {
                autoAlpha:0,
                display:"none",
                onComplete:() => {
                    this.clicking = true;
                }
            });
    }

    goBackToQuartenion() {
        if(!this.backToWorld) {
            this.controls.enabled = false;


            let currentPositionCamera = new THREE.Vector3(0,0,0).copy(this.threeCamera.position);


            let currentLookAtOfCamera = this.cameraLookAtEnd;


            new TWEEN.Tween(currentLookAtOfCamera).to(new THREE.Vector3( 0, 0, 0 ), 3000)
                .easing( TWEEN.Easing.Quadratic.Out )
                .onUpdate(() => {

                    this.cameraLookAt = currentLookAtOfCamera;

                })
                .onComplete(() => {
                })
                .start();

            new TWEEN.Tween(currentPositionCamera).to(new THREE.Vector3(-232.15169811828193,368.55975766436114,-300.03388733631255), 2500)
                .easing( TWEEN.Easing.Quadratic.Out )
                .onUpdate(() => {

                    this.threeCamera.position.set(currentPositionCamera.x,currentPositionCamera.y,currentPositionCamera.z)

                })
                .onComplete(() => {
                    this.globalScene = true;
                    this.currentOnIsland = true;
                    this.cameraIsAnimating = false;
                })
                .start();

        }
    }


  handleCameraPosition(width) {
    /*if (width < 768) {
      if(width < 420) {
        this.threeCamera.position.z = 800;
      } else {
        this.threeCamera.position.z = 600;
      }
    } else {
      this.threeCamera.position.z = 400;
    }*/
  }

    moveCameraWithPosition(target, object, callback) {
        this.isFirstMove = true;

        this.globalScene = false;
        this.cameraIsAnimating = true;
        this.currentOnIsland = false;

        if(this.backToWorld) {

            this.backToWorld = !this.backToWorld;

        }

        var target = target;

        // Vector of Camera

        var v = new THREE.Vector3(0,0,0).copy(this.threeCamera.position);

        let currentLookAtOfCamera = new THREE.Vector3( 0, 0, -1 );
        currentLookAtOfCamera.applyQuaternion( this.threeCamera.quaternion );

        // Manage type of movement
        let newObject = (object.parent.type !== "Scene") ? object.parent : object;


        // Update LookAt of Camera

        new TWEEN.Tween(currentLookAtOfCamera).to(new THREE.Vector3(newObject.position.x, newObject.position.y + 100, newObject.position.z), 2500)
            .easing( TWEEN.Easing.Quadratic.Out )
            .onUpdate(() => {

                this.cameraLookAt = currentLookAtOfCamera
                //console.log(currentLookAtOfCamera);

            })
            .onComplete(() => {
            })
            .start();
        // Update what the camera lookat for the render
        this.cameraLookAtEnd = new THREE.Vector3(newObject.position.x, newObject.position.y + 100, newObject.position.z);

        // Rotation around the object selected

        // Create a tween for the camera
        new TWEEN.Tween(v).to(target, 2500)
            .easing( TWEEN.Easing.Quadratic.Out )
            .onUpdate(() => {

                this.threeCamera.position.set(v.x, v.y, v.z)

            })
            .onComplete(() => {


                new TWEEN.Tween(v).to(new THREE.Vector3(target.x, target.y - 100, target.z), 1000)
                    .easing( TWEEN.Easing.Quadratic.InOut )
                    .onUpdate(() => {


                        this.threeCamera.position.set(v.x, v.y, v.z)

                    })
                    .onComplete(() => {
                        this.controls.enabled = true;
                        this.controls.target.copy(this.cameraLookAtEnd);
                        this.displayButtonExitIsland();
                        this.cameraIsAnimating = false;
                        if (callback !== null) { callback(); }
                    })
                    .start();
            })
            .start();


    }

    initEventMouse() {

        if(this.currentOnIsland) {

            // Start render which does not wait for model fully loaded
            this.renderer.domElement.addEventListener( 'mouseup', event => {
                this.down = false;
            });
            this.renderer.domElement.addEventListener( 'mousedown', event => {
                this.down = true;
            });

            this.renderer.domElement.addEventListener( 'mouseleave', event => {
                this.down  = false;
            });

            this.renderer.domElement.addEventListener( 'mousemove', event => {

                this.Bus.dispatch(this.Bus.types.ON_USER_DRAG, {drag: true});

                if (this.down && this.currentOnIsland) {

                    let delta = event.clientX - this.last.x;

                    // Rajouter un - négatif pour faire inverse

                    this.threeCamera.position.applyQuaternion(this.quaternion.setFromAxisAngle(
                        this.axis, (Math.PI * 2 * (delta / innerWidth) * 0.6)
                    ));

                    //this.camera.threeCamera.lookAt( this.scene.position );
                    this.last.set(event.clientX, 0);
                }

                if (this.last) {

                    let delta = event.clientY;
                    this.threeCamera.bottom = delta;

                }
            });

        }
    }

    onWindowResize() {
    // Update canvas renderer size
    this.renderer.setSize( window.innerWidth, window.innerHeight );

    // Check size
    this.handleCameraPosition(window.innerWidth);

    // Resize camera aspect
    this.threeCamera.aspect = window.innerWidth / window.innerHeight;
    this.threeCamera.updateProjectionMatrix();



  }

}
