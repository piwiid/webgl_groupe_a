/**
*
* CustomStats.js
*
**/


// Global dependencies
import * as THREE from 'three';
import Stats from '@jordandelcros/stats-js';

// Config dependencies
import Config from 'configs/Config';


export default class CustomStats {

	constructor(appContainer) {
		this.appContainer = appContainer;
		this.stats = new Stats(false);

		this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.top = '0px';
        this.stats.domElement.style.left = '0px';
        this.stats.domElement.style.top = '5px';
        this.stats.domElement.style.left = '5px';
        
        this.appContainer.appendChild(this.stats.domElement);
	}

	start() {
		if(Config.isDev) {
            this.stats.begin();
        }
	}

	stop() {
		if(Config.isDev) {
            this.stats.end();
        }
	}

}