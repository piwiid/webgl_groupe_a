/**
*
* CustomOrbitControls.js
*
**/


// Global imports
import * as THREE from 'three';
const OrbitControls = require('three-orbitcontrols')


export default class CustomOrbitControls {

	constructor(camera) {
		this.camera = camera;
		this.controls = new OrbitControls(this.camera.threeCamera);
		this.initOrbit();
	}

	initOrbit() {
		this.controls.update();
        this.controls.rotateSpeed = 1.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
        this.controls.minDistance = 200;
        this.controls.maxDistance = 300; // Need to adjust with final scene
        this.controls.enabled = false;
        this.controls.dampingFactor = 0.25;

        // Vertically rotate  (Y axis)
        this.controls.minPolarAngle = 0; // radians
        this.controls.maxPolarAngle = Math.PI/2; // radians
	}

}
