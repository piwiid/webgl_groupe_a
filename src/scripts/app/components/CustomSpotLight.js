/**
*
* CustomSpotLight.js
*
**/


// Global imports
import * as THREE from 'three';


export default class CustomSpotLight {

	constructor(scene) {
		this.scene = scene;
		
		this.light = new THREE.SpotLight( 0xffffff, 1 );
        this.light.position.set( 0, 1500, 200 );
        this.light.castShadow = true;

        this.light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 70, 1, 200, 400 ) );
        this.light.shadow.bias = -0.000222;
        this.light.shadow.mapSize.width = 1024;
        this.light.shadow.mapSize.height = 1024;
        
        this.scene.add(this.light);
	}

}
