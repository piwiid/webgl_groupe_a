/**
*
* Loader.js
*
**/

// Global dependencies
import * as THREE from 'three';

// Config dependencies
import Config from 'configs/Config';


export default class Loader {
  constructor() {
    const manager = new THREE.LoadingManager();
    const progressBar = document.querySelector( '#progress' );
    const loadingOverlay = document.querySelector( '#loading-overlay' );

    let percentComplete = 1;
    let frameID = null;

    const updateAmount = 0.5; // in percent of bar width, should divide 100 evenly

    const animateBar = () => {
      percentComplete += updateAmount;

      if ( percentComplete >= 100 ) {
        percentComplete = 1;
      }

      progressBar.style.width = percentComplete + '%';
      frameID = requestAnimationFrame( animateBar )
    }

    manager.onStart = () => {
      if ( frameID !== null ) return;
      animateBar();
    };


    manager.onLoad = function ( ) {
      loadingOverlay.classList.add( 'loading-overlay-hidden' );

      percentComplete = 0;
      progressBar.style.width = 0;
    
      cancelAnimationFrame( frameID );
    };
  

    manager.onError = function ( e ) { 
      console.error( e );
    }

    return manager;
  }

}
