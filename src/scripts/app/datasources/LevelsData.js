/**
*
* LevelsData.js
*
**/

// Global dependencies
import * as THREE from "three";

// Datas dependencies
import messagesData from "datasources/messagesData";
import modelsPersoPosition from "datasources/modelsPersoPosition";

export default {
    levels: {
        1: {
            isLevel: true,
            unlock: true,
            viewed: false,
            date: messagesData[1].date,
            message: messagesData[1].message,
            meshNumber: 5,
            positionPoint: { x: 164, y: 222, z: 245.9982 },
            rotationPoint: { x: 0, y: 0, z: 0 },
            positionCharacter: modelsPersoPosition[1],
            children: {
                1: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[2].date,
                    message: messagesData[2].message,
                    meshNumber: 1,
                    positionPoint: { x: 261, y: 82, z: 207 },
                    rotationPoint: { x: 0, y: 0, z: 0 },
                    positionCharacter: modelsPersoPosition[2],
                },
                2: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[3].date,
                    message: messagesData[3].message,
                    meshNumber: 1,
                    positionPoint: { x: 163, y: 82, z: 159 },
                    rotationPoint: { x: 0, y: 0, z: 0 },
                    positionCharacter: modelsPersoPosition[3],
                }
            }
        },
        2: {
            isLevel: true,
            unlock:false,
            viewed: false,
            date: messagesData[4].date,
            message: messagesData[4].message,
            meshNumber: 1,
            positionPoint: { x: -235, y: 222, z: 245.9982 },
            rotationPoint: { x: 0, y: 0, z: 0 },
            positionCharacter: modelsPersoPosition[4],
            children: {
                1: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[5].date,
                    message: messagesData[5].message,
                    meshNumber: 2,
                    positionPoint: { x: -227, y: 82, z: 179 },
                    rotationPoint: { x: 0, y: 0, z: 0 },
                    positionCharacter: modelsPersoPosition[5],
                },
                2: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[6].date,
                    message: messagesData[6].message,
                    meshNumber: 4,
                    positionPoint: { x: -157, y: 82, z: 200 },
                    rotationPoint: {x: 0, y: 0, z: 0},
                    positionCharacter: modelsPersoPosition[6],
                }
            }
        },
        3: {
            isLevel: true,
            unlock:false,
            viewed: false,
            date: messagesData[7].date,
            message: messagesData[7].message,
            meshNumber: 7,
            positionPoint: { x: 187, y: 222, z: -182 },
            rotationPoint: { x: 0, y: 0, z: 0 },
            positionCharacter: modelsPersoPosition[7],
            children: null
        },
        4: {
            isLevel: true,
            unlock:false,
            viewed: false,
            date: messagesData[8].date,
            message: messagesData[8].message,
            meshNumber: 3,
            positionPoint: { x: -213, y: 223, z: -189 },
            rotationPoint: { x: 0, y: 0, z: 0 },
            positionCharacter: modelsPersoPosition[8],
            children: {
                1: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[9].date,
                    message: messagesData[9].message,
                    meshNumber: 2,
                    positionPoint: { x: -163, y: 82, z: -189 },
                    rotationPoint: { x: 0, y: 0, z: 0 },
                    positionCharacter: modelsPersoPosition[9],
                },
                2: {
                    unlock: false,
                    viewed: false,
                    date: messagesData[10].date,
                    message: messagesData[10].message,
                    meshNumber: 2,
                    positionPoint: { x: -217, y: 82, z: -237 },
                    rotationPoint: { x: 0, y: 0, z: 0 },
                    positionCharacter: modelsPersoPosition[10],
                }
            }
        },
        5: {
            isLevel: true,
            unlock:false,
            viewed: false,
            date: messagesData[11].date,
            message: messagesData[11].message,
            meshNumber: 7,
            positionPoint: { x: 20, y: 482, z: 20 },
            rotationPoint: { x: 0, y: 0, z: 0 },
            positionCharacter: modelsPersoPosition[11],
            children: null
        }
    }
}
