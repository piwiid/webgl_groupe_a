/**
*
* ModelsBuildingData.js
*
**/


export default {
    1: {
        startActiveMesh: 'Natures',
        buildingMeshs: [
            {
                name: 'Trump_Tower',
                active: false,
            },
            {
                name: 'Ascenseur',
                active: false,
            },
            {
                name: 'Mails',
                active: false,
            },
            {
                name: 'Cache1',
                active: false,
            },
            {
                name: 'Cache2',
                active: false,
            },
            {
                name: 'FBI',
                active: false,
            },
            {
                name: 'Maison_Blanche',
                active: false,
            },
        ]
    },
    2: {
        startActiveMesh: 'Natures',
        buildingMeshs: [
            {
                name: 'Trump_Tower',
                active: false,
            },
            {
                name: 'Maison_Blanche',
                active: false,
            },
            {
                name: 'FBI',
                active: false,
            },
            {
                name: 'Cache1',
                active: false,
            },
            {
                name: 'Cache2',
                active: false,
            },
            {
                name: 'Mails',
                active: false,
            },
        ]
    },
    3: {
        startActiveMesh: 'Natures',
        buildingMeshs: [
            {
                name: 'Trump_Tower',
                active: false,
            },
            {
                name: 'Maison_Blanche',
                active: false,
            },
            {
                name: 'FBI',
                active: false,
            },
            {
                name: 'Cache1',
                active: false,
            },
            {
                name: 'Cache2',
                active: false,
            },
            {
                name: 'Mails',
                active: false,
            },
        ]
    },
    4: {
        startActiveMesh: 'Natures',
        buildingMeshs: [
            {
                name: 'Trump_Tower',
                active: false,
            },
            {
                name: 'Maison_Blanche',
                active: false,
            },
            {
                name: 'FBI',
                active: false,
            },
            {
                name: 'Cache1',
                active: false,
            },
            {
                name: 'Cache2',
                active: false,
            },
            {
                name: 'Mails',
                active: false,
            },
        ]
    },
    5: {
        startActiveMesh: 'Natures',
        buildingMeshs: [
            {
                name: 'Trump_Tower',
                active: false,
            },
            {
                name: 'Maison_Blanche',
                active: false,
            },
            {
                name: 'FBI',
                active: false,
            },
            {
                name: 'Cache1',
                active: false,
            },
            {
                name: 'Cache2',
                active: false,
            },
            {
                name: 'Mails',
                active: false,
            },
        ]
    }
}
