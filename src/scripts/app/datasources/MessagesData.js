/**
*
* MessagesData.js
*
**/

export default {
    1: {
        date: '8 novembre 2016',
        message: 'Donald Trump est élu président des Etats-Unis',
    },
    2: {
        date: 'Fin juillet 2016',
        message: 'Selon son témoignage, James Comey, directeur du FBI, entame une enquête sur une possible collusion entre la campagne Trump et la Russie.',
    },
    3: {
        date: '19 juillet 2016',
        message: 'Quatre mois avant son élection, Donald Trump devient officiellement le candidat républicain à la présidence. Ce même jour, il promeut Paul Manafort, le célèbre lobbyiste, à la tête de sa campagne.',
    },
    4: {
        date: 'Juillet - Novembre 2016',
        message: 'D’après une source proche du FBI, Paul Manafort est sous leur surveillance, depuis le début de la campagne présidentielle. Il est suspecté d’agir d\'une manière hostile aux États-Unis, en tant qu\'agent d’une puissance étrangère. Suite à cette surveillance, le FBI a découvert qu’il a gagné plusieurs millions de dollars en travaillant pour des intérêts russes, et qu’il a réalisé plusieurs investissements suspects dans des sociétés appartenant à des personnalités de la mafia russe.',
    },
    5: {
        date: '9 juin 2016',
        message: 'Seulement un mois après sa nomination au poste de directeur de campagne de Donald Trump, Manafort participe à une réunion secrète entre Donald Trump Junior et un avocat russe. <br> Officiellement, les trois hommes devaient parler d’une aide apportée aux orphelins russes. Cependant, il s\'avère que le gouvernement de Russie avait promis de fournir des informations préjudiciables sur Hillary Clinton, pour aider la campagne de Donald Trump.',
    },
    6: {
        date: '22 juillet 2016',
        message: 'Trump provoque un scandale juste avant les élections, en déclarant \"La Russie, si vous écoutez, j\'espère que vous pourrez trouver les 30 000 emails manquant.\". Le même jour, le site Wiklileaks publie 20.000 emails piratés du Comité National Démocrate, ce qui alimente l’idée que la Russie viendrait en aide à Donald Trump.',
    },
    7: {
        date: '3 août 2016',
        message: 'Suite à ce scandale, beaucoup de gens s’interroge sur le véritable motif de la réunion entre Donald Trump Junior, Manafort et l’avocat russe. Plusieurs photos et lettres reliant Manafort et un orphelinat en Ukraine pro-russe, prouvent que la réunion portait bien sur des enfants russes. Dans ces mêmes documents, plusieurs éléments laissent penser que Manafort agit en réalité sous les ordres du gouvernement Poutine, notamment pour aider Trump à atteindre le pouvoir. Le New York Times du 15 août 2016 rapporte en effet des paiements secrets et illégaux à Manafort de la part de groupes pro-russes.',
    },
    8: {
        date: 'Août 2016',
        message: 'Il y a quelques mois, deux anciens espions russes ont été éliminés à Londres. De récentes informations suggèrent que ces espions russes ont fuient leur pays à l’époque de l’élection américaine, et ont conclu un accord avec le MI6. Une fois placé sous protection, ils ont révélé une partie des plans du gouvernement russe. En réalité les orphelins ont tous été formés à l’espionnage, et envoyés dans le monde entier grâce à des adoptions, notamment aux États-Unis. Cela permet aujourd’hui à Poutine d’avoir une armée d’espions disponible pour surveiller toutes les instances des pays occidentaux.',
    },
    9: {
        date: '19 août 2016',
        message: 'Trump met brusquement fin à sa collaboration avec Manafort, seulement un mois après sa nomination à la tête de la campagne. Un courrier du gouvernement russe adressé à Manafort a été retrouvé par un journaliste indépendant. Ce document révèle que Manafort doit quitter son poste auprès de Trump pour devenir l’agent de liaison des enfants espions qui vont être adoptés aux USA. Il doit également faire en sorte que plusieurs enfants soient adoptés par des employés de la NASA.',
    },
    10: {
        date: 'Inconnue',
        message: 'Lors de son dernier lancement de télescope en orbite, le 18 juillet 2011, la Russie a découvert une étrange construction à la surface de la Lune. Quelques images prises par le télescope de cette mission RadioAstron ont fuitées à l’époque, malgré les efforts de la Russie pour les cacher. Poutine s\'intéresse donc de près à la Lune, ce qui fait de la NASA son principal concurrent.',
    },
    11: {
        date: 'Inconnue',
        message: 'D’après un scientifique russe réfugié politique,  la construction lunaire est en fait un laboratoire secret abandonné, construit par l\'Allemagne Nazie pendant la seconde guerre mondiale. Des documents trouvés après la chute du régime nazi parlent d’une machine en expérimentation, qui servirait à manipuler les foules grâce à l’émission d’ondes agissant sur le cortex préfrontal du cerveau humain. Placée sur la Lune, cette machine pourrait contrôler simultanément l’ensemble des êtres humains. <br> Notre source affirme que Poutine a trouvé cette machine incomplète sur la Lune et a envoyé des scientifiques sur place pour la terminer. Il doit donc éloigner les pays occidentaux de la Lune, jusqu’à ce que la machine soit terminée, et qu’il puisse asservir l’humanité !',
    }
}