/**
*
* ModelsData.js
*
**/

// Models dependencies
import level1 from "ressources/src3D/models/level1.obj";
import material1 from "ressources/src3D/materials/level1.mtl";

import level2 from "ressources/src3D/models/level2.obj";
import material2 from "ressources/src3D/materials/level2.mtl";

import level3 from "ressources/src3D/models/level3.obj";
import material3 from "ressources/src3D/materials/level3.mtl";

import level4 from "ressources/src3D/models/level4.obj";
import material4 from "ressources/src3D/materials/level4.mtl";

import level5 from "ressources/src3D/models/level5.obj";
import material5 from "ressources/src3D/materials/level5.mtl";


// Models Perso dependencies
import journaliste from "ressources/src3D/models/journaliste.obj";
import journalisteMaterial from "ressources/src3D/materials/journaliste.mtl";

// Datas dependencies
import modelsBuildingData from "datasources/modelsBuildingData";

// Utils const
const modelType = {
    building: 'building',
    perso: 'perso'
};

export default {
    modelsFile: [
        {
            model: level1,
            material: material1,
            type: modelType.building,
            modelData: modelsBuildingData[1],
            scale: 80,
        },
        {
            model: level2,
            material: material2,
            type: modelType.building,
            modelData: modelsBuildingData[2],
            scale: 80,
        },
        {
            model: level3,
            material: material3,
            type: modelType.building,
            modelData: modelsBuildingData[3],
            scale: 80,
        },
        {
            model: level4,
            material: material4,
            type: modelType.building,
            modelData: modelsBuildingData[4],
            scale: 80,
        },
        {
            model: level5,
            material: material5,
            type: modelType.building,
            modelData: modelsBuildingData[5],
            scale: 80,
        },
        {
            model: journaliste,
            material: journalisteMaterial,
            type: modelType.perso,
            modelData: null,
            scale: 10,
        }
    ]
}
