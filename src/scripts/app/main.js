/**
*
* Main.js
*
**/

// Global imports
import * as THREE from 'three';
import TWEEN from 'tween.js';

// Config dependencies
import Config from 'configs/Config';

// Managers
import ModelsManager from "managers/ModelsManager";

// Controllers
import RaycasterController from "controllers/RaycasterController";
import PointsController from "controllers/PointsController";

// Helpers
import EditorSpline from "helpers/utils/EditorSpline";

// Components
import CustomSpotLight from "components/CustomSpotLight";
import CustomOrbitControls from "components/CustomOrbitControls";
import CustomStats from "components/CustomStats";
import Camera from "components/Camera";
import Renderer from "components/Renderer";
import Univers from "components/Univers";
import { Bus } from "./helpers/events/Bus";
import StartAnimation from "./helpers/utils/StartAnimation";

export default class Main {
    constructor(container) {
        this.appContainer = container;

        // Init basic needed components
        this.lookAtObjects = [];  // Faire le load des Textures en premier -> Objet
        this.scene = new THREE.Scene();
        this.loadingManager = new THREE.LoadingManager();
        /*this.group = new THREE.Group();
        this.scene.add(this.group);*/

        this.animation = new StartAnimation();
        this.Bus = new Bus();

        this.initModelLoading();
    }

    initModelLoading() {
        this.models = new ModelsManager(this.scene, this.loadingManager);
        this.models.load();

        // Manage callback onProgress & onLoad (when finished).
        this.loadingManager.onProgress = function ( url, itemsLoaded, itemsTotal ) {
            //console.log( 'Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
        };

        this.loadingManager.onLoad = () => {

            // A voir pour la méthode load + add scene
            this.Bus.listen(this.Bus.types.ON_MODELS_LOAD, (params) => {
                if(params.load) {
                    this.initDevComponent();
                    this.initComponent();

                    this.render();
                }
            });
        }
    }

    initDevComponent() {
        if(Config.isDev) {
            //this.stats = new CustomStats(this.appContainer);
            //this.initEditor();
        }
    }

    initComponent() {
        this.container = document.createElement( 'div' );
        this.appContainer.appendChild( this.container );

        this.spotLight = new CustomSpotLight(this.scene);

        this.renderer = new Renderer(this.container);
        this.camera = new Camera(this.renderer.threeRenderer);
        this.camera.cameraLookAt = this.scene.position;

        this.orbitControls = new CustomOrbitControls(this.camera);
        this.camera.controls = this.orbitControls.controls;

        this.pointsController = new PointsController(this.scene, this.models, this.camera.controls);
        this.raycasterController = new RaycasterController(this.camera, this.models, this.pointsController);

        this.pointsController.initNewPoint();
    }

    initEditor() {
        this.initEditorSpline = new EditorSpline(this.scene, this.camera.threeCamera, this.orbitControls, this.renderer.threeRenderer);
    }


    render(time) {
        //this.stats.start();
        this.update();
        //this.stats.stop();

        // Request Animation Frame
        requestAnimationFrame(this.render.bind(this)); // Bind the main class instead of window object
    }

    update() {
        this.pointsController.updatePlanes(this.camera);
        TWEEN.update();
        //this.pointsController.checkModalIsDisplay(this.orbitControls);

        this.camera.threeCamera.lookAt(this.camera.cameraLookAt);
        this.renderer.threeRenderer.render(this.scene, this.camera.threeCamera);


        // this.initEditorSpline.updateSplineMesh();
    }
}
