/**
*
* RaycasterController.js
*
**/

// Global dependencies
import * as THREE from 'three';
import TWEEN from 'tween.js';
import {Bus} from "../helpers/events/Bus";


export default class RaycasterController {

	constructor(camera, models, pointsController) {
		this.camera = camera;
		this.models = models;
		
        this.pointsController = pointsController;

		this.mouse = new THREE.Vector2();
        this.raycaster = new THREE.Raycaster();

        this.Bus = new Bus();

        this.currentSelectedBuilding = '';

        this.initEvent();
	}
	
	initEvent() {
        document.addEventListener('mousedown', (e) => this.onDocumentMouseDown(e), false);
        //document.addEventListener('touchstart', (e) => this.onDocumentTouchStart(e), false);
    }

    checkAnimations() {
        this.cameraMoving = this.camera.cameraIsAnimating;
        this.modelDisplay = this.pointsController.modalIsDisplay;
        this.buildingAnimating = this.pointsController.buildingsAnimsController.buildingIsAnimating;
        this.persoAnimating = this.pointsController.persosAnimsController.persosIsAnimating;

        //console.log(this.cameraMoving, this.modelDisplay, this.buildingAnimating, this.persoAnimating);

        if (this.camera.isFirstMove && this.camera.globalScene) {
            this.currentSelectedBuilding = '';
        }

        if (this.cameraMoving || this.buildingAnimating || this.modelDisplay || this.persoAnimating) {
            return true;
        } else {
            return false;
        }
    }

    /*onDocumentTouchStart(event) {
        event.preventDefault();
        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        
        this.onDocumentMouseDown(event,index);
    }*/

    onDocumentMouseDown(event) {
        event.preventDefault();

        // Update position for rotate around the world
        this.camera.last = new THREE.Vector2( event.clientX, event.clientY );

        this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

        this.raycaster.setFromCamera(this.mouse, this.camera.threeCamera);

        let sceneObjectIntersects = this.raycaster.intersectObjects(this.models.objectRaycaster, true);
        let pointObjectIntersects = this.raycaster.intersectObjects(this.pointsController.pointObjects, true);

        // Check animations
        if (this.checkAnimations()) {
            return;
        }


        // Scene Raycaster
        if (sceneObjectIntersects.length > 0) {
            //setupTween(camera.position.clone(), new THREE.Vector3(intersects[0].point.x * 2, intersects[0].point.y * 2, intersects[0].point.z * 3), 800);
            //this.updateCameraPosition();

            this.Bus.dispatch(this.Bus.types.ON_USER_CLICK_TO_ISLAND, { click: true});


            let interObj = sceneObjectIntersects[0].object;
            
            let objClickable = this.models.buildingModel.find(item => item.name == interObj.parent.name).clickable;
            let targetPosition = this.getBuildingPosition(interObj.parent.name);


            if (interObj.parent.name !== this.currentSelectedBuilding && objClickable === true) {
                this.currentSelectedBuilding = interObj.parent.name;
                this.camera.moveCameraWithPosition(targetPosition, interObj, null);
            }

        }

        // Points raycaster
        if(pointObjectIntersects.length > 0) {
            let geometry = pointObjectIntersects[0].object.geometry;

            let obj = this.models.buildingModel.find(item => item.name == geometry.currentBuilding);
            let targetPosition = this.getBuildingPosition(geometry.currentBuilding);

            if (geometry.currentBuilding !== this.currentSelectedBuilding) {
                // Move and prepare point : Condition for first and previous click again
                this.currentSelectedBuilding = geometry.currentBuilding;
                this.camera.moveCameraWithPosition(targetPosition, obj, () => {
                    this.preparePointRaycaster(geometry, pointObjectIntersects);
                });
            } else {
                // Prepare point : Condition only for display message
                this.preparePointRaycaster(geometry, pointObjectIntersects);
            }
        }
    }


    // MARK : Helpers methods
    getBuildingPosition(buildingName) {
        let targetPosition = null;
        switch(buildingName) {
            case "building-level1":
                targetPosition = new THREE.Vector3(232.1516981182818, 368.5597576643611, 285.03388733631266);
                break;
            case "building-level2":
                targetPosition = new THREE.Vector3(-285.0338873363126, 368.5597576643611, 232.15169811828184);
                break;
            case "building-level3":
                targetPosition = new THREE.Vector3(285.0338873363127, 368.5597576643611, -232.15169811828173);
                break;
            case "building-level4":
                targetPosition = new THREE.Vector3(-232.15169811828167, 368.5597576643611, -285.0338873363128);
                break;
            case "building-level5":
                targetPosition = new THREE.Vector3(32.15169811828167, 568.5597576643611, 85.0338873363128);
                break;
            case "building-level":
                targetPosition = new THREE.Vector3(32.15169811828167, 568.5597576643611, 85.0338873363128);
                break;
        }
        return targetPosition;
    }

    preparePointRaycaster(geometry, pointObjectIntersects) {
        if (geometry.name.includes(this.pointsController.genericPointName) && geometry.isDisplayed == false) {
            geometry.isDisplayed = true;

            this.animatePoint(pointObjectIntersects);
            this.pointsController.displayModalDataView(false, null, geometry.currentChildrenLevel);
        } else {
            this.pointsController.displayModalDataView(true, geometry.currentLevel, geometry.currentChildrenLevel);
        }
    }

    animatePoint(pointObjectIntersects) {
        let material = new THREE.RawShaderMaterial({
                uniforms: {
                    time: {value: 0},
                    opacity: {value: 0},
                    hover: {value: 0}
                },
                transparent: true,
                vertexShader: document.getElementById('vertexShader').textContent,
                fragmentShader: document.getElementById('fragmentShaderPurple').textContent
            });

            new TWEEN.Tween({ opacity: 1}) // Create a new tween that modifies 'coords'.
                .to({ opacity: 0}, 500) // Move to (300, 200) in 1 second.
                 .easing(TWEEN.Easing.Quadratic.Out) // Use an easing function to make the animation smooth.
                .onUpdate(function() { // Called after tween.js updates 'coords'.
                    // Move 'box' to the position described by 'coords' with a CSS translation.
                    pointObjectIntersects[0].object.material.uniforms.opacity.value = this.opacity;
                })
                .onComplete(() => {
                    pointObjectIntersects[0].object.material = material;
                    
                    new TWEEN.Tween({ opacity: 0}) // Create a new tween that modifies 'coords'.
                        .to({ opacity: 1}, 500) // Move to (300, 200) in 1 second.
                        .easing(TWEEN.Easing.Quadratic.Out) // Use an easing function to make the animation smooth.
                        .onUpdate(function() { // Called after tween.js updates 'coords'.
                            // Move 'box' to the position described by 'coords' with a CSS translation.
                            pointObjectIntersects[0].object.material.uniforms.opacity.value = this.opacity;
                        })
                    .start();

                })
                .start();
    }
}
