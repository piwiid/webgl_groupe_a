/**
*
* PointsController.js
*
**/

// Global dependencies
import * as THREE from 'three';

// Managers dependencies
import MessagesDataManager from 'managers/MessagesDataManager';

import PersosAnimsController from "controllers/animations/PersosAnimsController";
import BuildingsAnimsController from "./animations/BuildingsAnimsController";


export default class PointsController {

	constructor(scene, models, controls) {
		this.scene = scene;
        this.models =  models;
        this.controls = controls;

		this.messagesDataManager = new MessagesDataManager();

		this.buildingsAnimsController = new BuildingsAnimsController(this.models);
        this.persosAnimsController = new PersosAnimsController(this.models.journaliste);

		this.currentLevel =  1;
		this.pointObjects = [];
		this.genericPointName = 'Point-';
		this.pointNumber = 0;
        this.planes = [];

        this.level = 0;


		this.clickHandler = this.onModalClick.bind(this);
        this.modalIsDisplay = false;
	}

    updatePlanes(camera) {
        this.planes.forEach(e => {
            let conj = new THREE.Quaternion();
            conj.copy(this.scene.quaternion);
            conj.conjugate();

            e.quaternion.multiplyQuaternions(
                conj,
                camera.threeCamera.quaternion
            );

            // e.quaternion.copy(camera.quaternion);
        });
    }

	// Create New Point
	initNewPoint() {
        var currentInfos = this.messagesDataManager.getCurrentLevelOrChildrens();


        currentInfos.forEach((element, index) => {
            this.pointNumber += 1;

            var positionPoint = element.positionPoint;
            var rotationPoint = element.rotationPoint;

            let geometry = new THREE.PlaneGeometry( 14,14 );
            let material = new THREE.MeshBasicMaterial( {
                color: 0x00ff00,
                side: THREE.DoubleSide,

                // wireframe: true
            } );
            let material1 = new THREE.RawShaderMaterial( {
                uniforms: {
                    opacity: {value: 1},
                    time: {value: 0},
                    hover: {value: 0}
                },
                transparent: true,
                vertexShader: document.getElementById('vertexShader').textContent,
                fragmentShader: document.getElementById('fragmentShaderBlue').textContent
            } );
            let plane = new THREE.Mesh(geometry,material1);


            geometry.name = this.genericPointName + this.pointNumber;
            geometry.pointNumber = this.pointNumber;
            geometry.isDisplayed = false;
            geometry.currentBuilding = "building-level" + this.messagesDataManager.getCurrentLevel();
            geometry.currentLevel = this.messagesDataManager.getCurrentLevel();
            geometry.currentChildrenLevel = element.isLevel !== true ? index+1 : null;

            var pointMaterial = new THREE.MeshBasicMaterial( { color: 0xD3D3D3 } );
            var pointMesh = new THREE.Mesh( geometry, pointMaterial );

            pointMesh.scale.x = pointMesh.scale.y = pointMesh.scale.z = 0.8;

            plane.position.set(positionPoint.x, positionPoint.y, positionPoint.z);

            this.scene.add(plane);
            this.planes.push(plane);

            this.pointObjects.push(plane);

            console.log("plane",this.pointObjects);
        });
    }

    // Show informations methods
    displayModalDataView(alwaysDisplay, levelID, childrenID) {
 		let currentInfos = (alwaysDisplay === true) 
 			? this.messagesDataManager.getSpecificLevelOrChildren(levelID, childrenID) 
 			: this.messagesDataManager.getCurrentLevelOrChildrens();

        currentInfos.forEach((element, index) => {
            let myIndex = index+1;

            // Level without child
            if (childrenID == null) {
            	myIndex = null;
            	this.constructModalView(element, alwaysDisplay, myIndex);
            } else {
            	// Children matchs when wanted clicked
		        if (childrenID == myIndex && alwaysDisplay == false) {
                    this.constructModalView(element, alwaysDisplay, myIndex);
		        } else if (alwaysDisplay == true) {
                    this.constructModalView(element, alwaysDisplay, childrenID);
                }
            }
        });
    }

    // MARK : Construct methods
    constructModalView(element, alwaysDisplay, index) {
    	let newIndex = (index === null) ? 1 : index;

    	let dateText = document.getElementById('infos-title-' + newIndex);
    	dateText.textContent = element.date;

        let messageText = document.getElementById('infos-message-' + newIndex);
        messageText.textContent = element.message;

        this.clickHandler = this.onModalClick.bind(this, element, alwaysDisplay, newIndex);

        let continueButton = document.getElementById('continueButton-' + newIndex);
        continueButton.addEventListener('click', this.clickHandler, true);

        var infosModal = document.getElementById('infos-modal-' + newIndex);
        infosModal.style.display = 'flex';

        this.modalIsDisplay = true;
    }

    // MARK : onClick methods
    onModalClick(element, alwaysDisplay, newIndex) {
    	if (alwaysDisplay == false) {
    		this.confirmModalDataView(element, newIndex);
    	}
    	this.hideModalDataView(newIndex);
    }

    // MARK : Helpers listeners methods
    hideModalDataView(index) {
    	let infosModal = document.getElementById('infos-modal-' + index);
        infosModal.style.display = 'none';

        let continueButton = document.getElementById('continueButton-' + index);
        continueButton.removeEventListener('click', this.clickHandler, true);

        this.modalIsDisplay = false;
    }

    confirmModalDataView(element, index) {
        var isNext = this.messagesDataManager.setViewedCurrentLevelOrChildren(index);

        if (isNext) {
            this.messagesDataManager.getNextLevel();
            var nextLevel = this.messagesDataManager.getCurrentLevel();

            if (this.currentLevel !== nextLevel) {
                this.playIntrinsicAnims(element, false, () => {
                    this.childrenIsChooseForAnim = undefined;
                    this.currentLevel = nextLevel;
                    this.playBuildingAnims();
                });
            } else {
                this.playIntrinsicAnims(element, true, null);   
            }
        } else {
            this.playIntrinsicAnims(element, false, null);
        }
    }

    playBuildingAnims() {
        this.buildingsAnimsController.displayBuildingAnimations(this.currentLevel, () => {
            this.persosAnimsController.moveCharacterToNextLevel(this.currentLevel);
            this.initNewPoint();
        });
    }

    playIntrinsicAnims(element, initPoint, callback) {
        this.preparePersoAnimations(element);

        this.buildingsAnimsController.displayIntrinsicBuildingAnimation(element, this.currentLevel, () => {
            //console.log('first')
            this.persosAnimsController.updatePositionOfCharacter( () => {
                if (initPoint == true) { this.initNewPoint(); }
                if (callback !== null) { callback(); }
            });
        });
    }

    preparePersoAnimations(element) {
        // Faire condition quand on va sur les childs. Si tu veux faire des Splines il faut activer le editorSpline
        if (element.children === undefined && this.childrenIsChooseForAnim === undefined) {
            let position = element.positionCharacter.pointsNormalPath;
            //console.log('first child', position);
            this.preparePosition(position);
            this.childrenIsChooseForAnim = true;
        } else {
            if (this.childrenIsChooseForAnim === true) {
                let position = element.positionCharacter.pointsEscapePath;
                //console.log('second child', position);
                this.preparePosition(position);
            } else {
                let position = element.positionCharacter.pointsNormalPath;
                //console.log('main level', position);
                this.preparePosition(position);
            }
        }  
    }

    preparePosition(position) {
        this.initialSplinePosition = new THREE.CatmullRomCurve3(position);
        this.splinePoints = this.initialSplinePosition.getPoints(500);
        this.persosAnimsController.splinePoints = this.splinePoints;
    }

}
