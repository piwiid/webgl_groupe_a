/**
*
* BuildingsAnimsController.js
*
**/

// Global dependencies
import * as THREE from 'three';
import TWEEN from 'tween.js';

// Managers dependencies
import ModelsBuildingDataManager from 'managers/ModelsBuildingDataManager';


export default class BuildingsAnimsController {

	constructor(models) {
		this.models = models;
    
        this.buildingIsAnimating = false;
        this.modelsBuildingDataManager = new ModelsBuildingDataManager(this.models);
	}


	displayBuildingAnimations(level, callback) {
        let buildingToAppear = this.modelsBuildingDataManager.getBuildingData(level);

        // Make transition
        //console.log(this.models.buildingModel);
        let targetY = (buildingToAppear.name === "building-level5") ? 400 : 0;

        let position = { x : buildingToAppear.position.x , y: buildingToAppear.position.y };
        let target = { x : buildingToAppear.position.x, y: targetY };
        let tween = new TWEEN.Tween(position).to(target, 4000);

        tween.onUpdate(function(){
            buildingToAppear.position.x = position.x;
            buildingToAppear.position.y = position.y;
        });

        tween.delay(0);
        tween.easing(TWEEN.Easing.Cubic.In);

        // Make model visible and start animation
        this.buildingIsAnimating = true;
        buildingToAppear.visible = true;
        tween.start();

        tween.onComplete(() => {
            this.buildingIsAnimating = false;
            buildingToAppear.clickable = true;
            callback();
        });
    }

    displayIntrinsicBuildingAnimation(element, level, callback) {
        let meshNumber = element.meshNumber;
        let meshMax = this.modelsBuildingDataManager.getMeshCount(level);

        if (meshNumber > meshMax) {
            meshNumber = meshMax;
        }

        if (meshNumber === null || meshNumber == undefined) {
            callback();
        }

        let tweens = [];
        let animationTime = 2000;
        for (var i = 0; i < meshNumber; i++) {
            let buildingToConstruct = this.modelsBuildingDataManager.getBuildingData(level);
            let displayElement = this.modelsBuildingDataManager.getElementToDisplayed(buildingToConstruct, level);

            if (displayElement !== null) {
                this.modelsBuildingDataManager.setElementToDisplayed(buildingToConstruct, level);

                let position = { x : displayElement.position.x , y: displayElement.position.y, opacity: displayElement.material.opacity};
                let target = { x : displayElement.position.x, y: 0, opacity: 1 };

                tweens.push(new TWEEN.Tween(position).to(target, animationTime));
                animationTime += 1000;

                tweens[tweens.length-1].onUpdate(function() {
                    displayElement.position.x = position.x;
                    displayElement.position.y = position.y;
                    displayElement.material.opacity = position.opacity;
                });

                 // Make model visible and start animation
                this.buildingIsAnimating = true;
                displayElement.visible = true;

                tweens[tweens.length-1].delay(0);
                tweens[tweens.length-1].easing(TWEEN.Easing.Quadratic.Out);
                tweens[tweens.length-1].start();

                //this.fadeMesh(displayElement, "in");

                if (i == meshNumber-1) {
                    tweens[tweens.length-1].onComplete(() => { 
                        this.buildingIsAnimating = false;
                        callback();
                    });
                }
            } else {
                callback();
                return;
            }
        } 
    }
    
}