/**
*
* PersosAnimsController.js
*
**/

// Global dependencies
import * as THREE from 'three';


export default class PersosAnimsController {

	constructor(character) {

		this.character = character;
		this.persosIsAnimating = false;
		this.characterAdvancement = 0;
		this.splinePoints = null;
	}

    getPosition(splinePoints) {
        let scale = 100;


        let point = Math.floor(this.splinePoints.length * this.characterAdvancement + this.splinePoints.length) % this.splinePoints.length;
        let characterPosition = this.splinePoints[point]
        return {
            x: characterPosition.x,
            y: characterPosition.y,
            z: characterPosition.z,
        }
    }

    moveCharacterToNextLevel(level) {
        switch(level) {
            case 2: 
                this.character.position.set(-200, 72, 200);
                break;
            case 3: 
                this.character.position.set(200, 72, -200);
                break;
            case 4: 
                this.character.position.set(-200, 72, -200);
                break;
            case 5: 
                this.character.position.set(0, 472, 0);
                break;
            default:
                this.character.position.set(1000, 1000, 1000);
        }
    }

    updatePositionOfCharacter(callback) {
    	this.persosIsAnimating = true;

        if(this.characterAdvancement >= 0.99) {
            cancelAnimationFrame(this.animationCharacter);
            this.persosIsAnimating = false;
            this.characterAdvancement = 0;
            callback();
        } else {
            this.characterAdvancement += 0.002;
            let t = this.getPosition();

            this.character.position.set(t.x, t.y, t.z);

            this.animationCharacter = requestAnimationFrame(this.updatePositionOfCharacter.bind(this, callback));
        }
    }
}
