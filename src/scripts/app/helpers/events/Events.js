/**
*
* Events.js
*
**/


export const Events = {
	PERCENTAGE_OF_LOAD: "percentageOfLoad",
	ON_USER_DRAG: 'onUserDrag',
	ON_MODELS_LOAD: 'onModelsLoad',
	CURRENT_ISLAND: 'currentIsland',
	ON_USER_CLICK_TO_ISLAND: 'onUserClickToIsland',
}
