/**
*
* EditorSpline.js
*
**/

// Global dependencies
import * as THREE from 'three';

String.prototype.format = function () {
    var str = this;
    for ( var i = 0; i < arguments.length; i ++ ) {
        str = str.replace( '{' + i + '}', arguments[ i ] );
    }
    return str;
};


export default class EditorSpline {
    constructor(scene,camera, controls, renderer) {

        this.scene = scene;
        this.camera= camera;
        this.controls = controls;
        this.renderer = renderer;

        // Faire le load des Textures en premier -> Objet
        THREE.Cache.enabled = true;

        this.scrollPosition = 0



        this.splinePosition = new THREE.CatmullRomCurve3(
            [new THREE.Vector3(190.20917045977563, 235.68155369005092, 231.7366188962488),
                new THREE.Vector3(199.75748216340276, 82.56538180663505, 237.92404304908854),
                new THREE.Vector3(199.75748216340276, 82.56538180663505, 210.98357641912693),
                new THREE.Vector3(234.01226327607586, 82.64942249031351, 209.11680908330797)]
        );

        this.lookAtObjects = []
        this.point = new THREE.Vector3();
        this.splineHelperObjects = []
        this.splinePointsLength = 4;
        this.positions = [];
        this.point = new THREE.Vector3();

        this.geometry = new THREE.BoxBufferGeometry( 20, 20, 20 );
        this.loader = false;

        this.ARC_SEGMENTS = 200;
        this.splines = {};
        this.params = {
            uniform: true,
            tension: 0.5,
            centripetal: true,
            animationView: false,
            chordal: true,
            lookAhead: false,
            cameraHelper: false,
            addPoint: () => this.addPoint(),
            removePoint: () => this.removePoint(),
            exportSpline: () => this.exportSpline()
        };
        this.geometry = new THREE.BoxBufferGeometry( 20, 20, 20 );

        var gui = new dat.GUI();
        gui.add( this.params, 'uniform' );
        gui.add( this.params, 'tension', 0, 1 ).step( 0.01 ).onChange( ( value ) => {
            this.splines.uniform.tension = value;
            this.updateSplineOutline();
        });
        gui.add( this.params, 'centripetal' );
        gui.add( this.params, 'chordal' );
        gui.add( this.params, 'addPoint' );
        gui.add( this.params, 'removePoint' );
        gui.add( this.params, 'exportSpline' );
        gui.open();

        gui.add( this.params, 'animationView').onChange( ( value ) => { this.animateCamera(); } );


        this.init();

        this.splinePoints = this.splinePosition.getPoints(500);

        this.buildFromLine();

        /*raycaster = new THREE.Raycaster();*/
    }

    buildFromLine() {
        var geometry = new THREE.Geometry();
        var material = new THREE.LineBasicMaterial({
            color: 0xff00f0,
        });

        for (var i = 0; i < this.splinePoints.length; i++) {
            geometry.vertices.push(this.splinePoints[i]);

        }

        var line = new THREE.Line(geometry, material);
        this.scene.add(line);

    }

    init() {


        this.scene.add( new THREE.AmbientLight( 0xf0f0f0 ) );
        var light = new THREE.SpotLight( 0xffffff, 1.5 );
        light.position.set( 0, 1500, 200 );
        light.castShadow = true;
        light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 70, 1, 200, 2000 ) );
        light.shadow.bias = -0.000222;
        light.shadow.mapSize.width = 1024;
        light.shadow.mapSize.height = 1024;
        this.scene.add( light );
        this.spotlight = light;


        var planeGeometry = new THREE.PlaneBufferGeometry( 2000, 2000 );
        planeGeometry.rotateX( - Math.PI / 2 );
        var planeMaterial = new THREE.ShadowMaterial( { opacity: 0.2 } );
        var plane = new THREE.Mesh( planeGeometry, planeMaterial );
        plane.position.y = -200;
        plane.receiveShadow = true;
        this.scene.add( plane );
        var helper = new THREE.GridHelper( 2000, 100 );
        helper.position.y = - 199;
        helper.material.opacity = 0.25;
        helper.material.transparent = true;
        this.scene.add( helper );

        /*this.controls.addEventListener( 'start', () => {
            this.cancelHideTransorm();
        } );
        this.controls.addEventListener( 'end', () => {
            this.delayHideTransform();
        } );*/

        //console.log("Salut", this.renderer.threeRenderer.domElement);
        this.transformControl = new THREE.TransformControls( this.camera, this.renderer.domElement);



        this.scene.add( this.transformControl );
        // Hiding transform situation is a little in a mess :()
        this.transformControl.addEventListener( 'change', ( e ) => {
            this.cancelHideTransorm();
        } );
        this.transformControl.addEventListener( 'mouseDown', ( e ) => {
            this.controls.enabled = false;
            this.cancelHideTransorm();
        } );
        this.transformControl.addEventListener( 'mouseUp', ( e ) => {
            this.controls.enabled = true;
            this.delayHideTransform();
        } );
        this.transformControl.addEventListener( 'objectChange', ( e ) => {
            this.updateSplineOutline();
        } );

        this.dragcontrols = new THREE.DragControls( this.splineHelperObjects, this.camera, this.renderer.domElement ); //

        this.dragcontrols.addEventListener( 'hoveron',  ( event ) => {
            this.transformControl.attach( event.object );
            this.cancelHideTransorm();
        } );
        this.dragcontrols.addEventListener( 'hoveroff', ( event ) => {
            this.delayHideTransform();
        } );


        for ( var i = 0; i < this.splinePointsLength; i ++ ) {
            this.addSplineObject( this.positions[ i ] );
        }


        for ( var i = 0; i < this.splinePointsLength; i ++ ) {
            this.positions.push( this.splineHelperObjects[ i ].position );
        }
        var geometry = new THREE.BufferGeometry();
        geometry.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array(this.ARC_SEGMENTS * 3 ), 3 ) );
        var curve = new THREE.CatmullRomCurve3( this.positions );
        curve.curveType = 'catmullrom';
        curve.mesh = new THREE.Line( this.geometry.clone(), new THREE.LineBasicMaterial( {
            color: 0xff0000,
            opacity: 0.35
        } ) );
        curve.mesh.castShadow = true;
        this.splines.uniform = curve;
        curve = new THREE.CatmullRomCurve3( this.positions );
        curve.curveType = 'centripetal';
        curve.mesh = new THREE.Line( this.geometry.clone(), new THREE.LineBasicMaterial( {
            color: 0x00ff00,
            opacity: 0.35
        } ) );
        curve.mesh.castShadow = true;
        this.splines.centripetal = curve;
        curve = new THREE.CatmullRomCurve3( this.positions );
        curve.curveType = 'chordal';
        curve.mesh = new THREE.Line( this.geometry.clone(), new THREE.LineBasicMaterial( {
            color: 0x0000ff,
            opacity: 0.35
        } ) );
        curve.mesh.castShadow = true;
        this.splines.chordal = curve;
        for ( var k in this.splines ) {
            var spline = this.splines[ k ];
            this.scene.add( spline.mesh );
        }

        this.load(
            [new THREE.Vector3(190.20917045977563, 235.68155369005092, 231.7366188962488),
                new THREE.Vector3(199.75748216340276, 82.56538180663505, 237.92404304908854),
                new THREE.Vector3(199.75748216340276, 82.56538180663505, 210.98357641912693),
                new THREE.Vector3(234.01226327607586, 82.64942249031351, 209.11680908330797)]
        );
    }

    addSplineObject( position ) {
        var material = new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } );
        var object = new THREE.Mesh( this.geometry, material );
        if ( position ) {
            object.position.copy( position );
        } else {
            object.position.x = Math.random() * 1000 - 500;
            object.position.y = Math.random() * 600;
            object.position.z = Math.random() * 800 - 400;
        }
        object.castShadow = true;
        object.receiveShadow = true;
        this.scene.add( object );
        this.splineHelperObjects.push( object );
        return object;
    }

    delayHideTransform() {
        this.cancelHideTransorm();
        this.hideTransform();
    }

    cancelHideTransorm() {
        if ( this.hiding ) clearTimeout( this.hiding );
    }
    hideTransform() {
        this.hiding = setTimeout( () => {
            this.transformControl.detach( this.transformControl.object );
        }, 2500 )
    }

    addPoint() {

            this.splinePointsLength ++
            this.positions.push( this.addSplineObject().position );
            this.updateSplineOutline();
    }

    removePoint() {

        if(this.loader) {
            if ( this.splinePointsLength <= 4 ) {
                return;
            }
            this.splinePointsLength --;
            this.positions.pop();
            this.scene.remove( this.splineHelperObjects.pop() );
            this.updateSplineOutline();
        }
    }

    updateSplineOutline() {
        for ( var k in this.splines ) {
            var spline = this.splines[ k ];
            var splineMesh = spline.mesh;
            var position = splineMesh.geometry.attributes.position;
            for ( var i = 0; i < this.ARC_SEGMENTS; i ++ ) {
                var t = i /  (this.ARC_SEGMENTS - 1 );
                spline.getPoint( t, this.point );
                position.setXYZ( i, this.point.x, this.point.y, this.point.z );
            }
            position.needsUpdate = true;

        }
        this.splines.uniform.mesh.visible = this.params.uniform;
        this.splines.centripetal.mesh.visible = this.params.centripetal;
        this.splines.chordal.mesh.visible = this.params.chordal;

    }

    exportSpline() {

            var strplace = [];
            for ( var i = 0; i < this.splinePointsLength; i ++ ) {
                var p = this.splineHelperObjects[ i ].position;
                strplace.push( 'new THREE.Vector3({0}, {1}, {2})'.format( p.x, p.y, p.z ) )
            }
            console.log( strplace.join( ',\n' ) );
            var code = '[' + ( strplace.join( ',\n\t' ) ) + ']';
            prompt( 'copy and paste code', code );
    }

    load( new_positions ) {
        while (new_positions.length > this.positions.length) {
            addPoint();
        }
        while (new_positions.length < this.positions.length) {
            this.removePoint();
        }
        for (var i = 0; i < this.positions.length; i++) {
            this.positions[i].copy(new_positions[i]);
        }
        this.updateSplineOutline();
    }

    updateSplineMesh() {
        this.splines.uniform.mesh.visible = this.params.uniform;
        this.splines.centripetal.mesh.visible = this.params.centripetal;
        this.splines.chordal.mesh.visible = this.params.chordal;
    }


}
