import SplitText from 'ressources/libs/SplitText.min'
import {TweenLite, TimelineLite, TimelineMax} from 'gsap'
import {Bus} from "../events/Bus";

export default class StartAnimation {

    constructor() {

        this.tl = new TimelineLite;
        this.loaderSvg = new TimelineMax({
            repeat: -1,
        });

        this.Bus = new Bus();

        this.initLoader();


        this.startButton = document.getElementById('startButton');
        this.startButton.disabled = true;
        this.startButton.addEventListener('click', () => {
            this.hideStartUI();

        });

        this.firstDrag = true;
        this.firstClickToIsland = true;

    }


    init() {

        // Init UI
        this.initPresentationNewspaper();
        this.initPresentation();
    }

    initLoader() {

        let startPercentage  = {
            value: 0,
        }

        this.Bus.listen(this.Bus.types.PERCENTAGE_OF_LOAD, (param) => {

            this.tl.to(startPercentage, 0.2, {value:param.percentage, roundProps:"value",
                onUpdate: () => {

                    document.querySelector('.loading__number').innerHTML = startPercentage.value + "%";

                },
                onComplete: () => {

                    if (startPercentage.value === 100) {

                        this.mySplitText = new SplitText(".teamContainer h2", {type:"words,chars"});
                        this.chars = this.mySplitText.chars;
                        this.mySplitText2 = new SplitText(".teamContainer p", {type:"words,chars"});
                        this.chars2 = this.mySplitText2.chars;
                        this.mySplitText3 = new SplitText(".titleContainer h1", {type:"words,chars"});
                        this.chars3 = this.mySplitText3.chars;
                        this.mySplitText4 = new SplitText(".titleContainer h2", {type:"words,chars"});
                        this.chars4 = this.mySplitText4.chars;
                        this.displayLoader();

                    }

                },
                ease:Linear.easeNone
            });
        })

        var drop = document.getElementById('drop');
        var drop2 = document.getElementById('drop2');
        var outline = document.getElementById('outline');

        this.loaderSvg.set(drop, {
            transformOrigin: '50% 50%'
        })


        this.loaderSvg.timeScale(3);

        this.loaderSvg.to(drop, 4, {
            attr: {
                cx: 250,
                rx: '+=10',
                ry: '+=10'
            },
            ease: Back.easeInOut.config(3)
        })
            .to(drop2, 4, {
                attr: {
                    cx: 250
                },
                force3D:true,
                ease: Power1.easeInOut
            }, '-=4')
            .to(drop, 4, {
                attr: {
                    cx: 125,
                    rx: '-=10',
                    ry: '-=10'
                },
                force3D:true,
                ease: Back.easeInOut.config(3)
            })
            .to(drop2, 4, {
                attr: {
                    cx: 125,
                    rx: '-=10',
                    ry: '-=10'
                },
                force3D:true,
                ease: Power1.easeInOut
            }, '-=4')









    }
    displayLoader() {

        this.tl.staggerTo('.loading__number', 1.5, {
            opacity:0,
            y:40,
            ease: Quart.easeIn,
            force3D:true,
            onComplete: () => {
                this.tl.fromTo('.loading', 1, { autoAlpha: 1,display:"flex",force3D:true,}, {
                    autoAlpha: 0,
                    display: "none",
                    force3D:true,
                    onComplete:() => {

                        // Kill Loader
                        this.loaderSvg.stop();
                        this.loaderSvg.kill();

                        this.init();


                    }});
            }
        }, 0, "-=0.0");

    }

    initPresentationNewspaper() {

        this.tl.set("#mainContainer", {
            display: 'flex',
        })

        this.tl.staggerFrom(this.chars, 1.5, {
            opacity:0,
            y:40,
            force3D:true,
            ease: Quart.easeOut
        }, 0, "+=0.0");
        this.tl.staggerFrom(this.chars2, 1.5, {
            opacity:0,
            y:40,
            force3D:true,
            ease: Quart.easeOut,
        }, 0, "-=1.3");

        this.tl.staggerTo(this.chars, 1.5, {
            opacity:0,
            y:-40,
            force3D:true,
            ease: Quart.easeIn,
        }, 0, "+=0.0");
        this.tl.staggerTo(this.chars2, 1.5, {
            opacity:0,
            y:-40,
            force3D:true,
            ease: Quart.easeIn,
        }, 0, "-=1.2");

    }
    initPresentation() {

        this.tl.set(".titleContainer", { opacity: 1});

        this.tl.staggerFrom(this.chars3, 1.5, {
            opacity:0,
            y:40,
            force3D:true,
            ease: Quart.easeOut
        }, 0, "+=0.0");
        this.tl.staggerFrom(this.chars4, 1.5, {
            opacity:0,
            y:40,
            force3D:true,
            ease: Quart.easeOut,
        }, 0, "-=1.3");

        this.tl.staggerTo(this.chars3, 1.5, {
            opacity:0,
            y:-40,
            force3D:true,
            ease: Quart.easeIn,
        }, 0, "+=0.0");
        this.tl.staggerTo(this.chars4, 1.5, {
            opacity:0,
            y:-40,
            force3D:true,
            ease: Quart.easeIn,
        }, 0, "-=1.2");

        // Présente

        this.tl.to('.presentation',1, {
            autoAlpha: 1,
            force3D:true,
        }, "-=3.5")

        this.tl.staggerFrom(".context", 1.5, {
            autoAlpha:0,
            force3D:true,
            ease: Quart.easeOut,
        }, "-=5.5");

        this.tl.staggerFrom(".context .content", 1.5, {
            opacity:0,
            y:80,
            force3D:true,
            ease: Quart.easeOut,
        }, "-=1.0");
        document.querySelector('.content a').addEventListener('click', () => {
            this.tl.to('.presentation',1, {
                autoAlpha: 1,
                force3D:true,
            })
            this.tl.staggerTo(".context .content", 1.5, {
                opacity:0,
                y:-40,
                force3D:true,
                ease: Quart.easeIn,
            }, 0, "-=1.0");

        });
    }


    hideStartUI() {

        this.tl.fromTo('#mainContainer', 1,{autoAlpha:1,force3D:true,}, {autoAlpha:0,display:'none',force3D:true,});
        this.tl.to('.panel-drag', 1,{autoAlpha:1,force3D:true,onComplete:this.hideDragUI.bind(this)});
        this.tl.to('.panel-clic', 1,{autoAlpha:1,force3D:true, onComplete: this.hideClickIslandUI.bind(this)});
        this.tl.to('.panel-logo', 1,{autoAlpha:1,force3D:true,});
        this.tl.to('.panel-information', 1,{autoAlpha:1,force3D:true,});
    }


    hideDragUI() {
        this.Bus.listen(this.Bus.types.ON_USER_DRAG, (param) => {

            if(param.drag && this.firstDrag) {
                this.tl.to('.panel-drag', 1, {
                    delay:1,
                    autoAlpha: 0,
                    force3D:true,
                })
                this.firstDrag = false;
            }

        })
    }

    hideClickIslandUI() {

        this.Bus.listen(this.Bus.types.ON_USER_CLICK_TO_ISLAND, (param) => {

            if(param.click && this.firstClickToIsland) {
                this.tl.to('.panel-clic', 1, {
                    delay:1,
                    autoAlpha: 0,
                    force3D:true,
                })
                this.firstClickToIsland = false;
            }
        })
    }



}
