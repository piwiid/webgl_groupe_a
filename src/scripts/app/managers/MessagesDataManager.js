/**
*
* MessagesDataManager.js
*
**/

// Datas dependencies
import levelsData from "datasources/levelsData";

export default class MessagesDataManager {
	constructor() {
		this.currentLevel = 1;
		this.currentChildren = null;
		this.levelsData = levelsData.levels;
    }

    increaseCurrentLevel() {
        this.currentLevel += 1;
    }

    getCurrentLevel() {
        return this.currentLevel;
    }

    /*getCurrentChildren() {
        return this.currentChildren;
    }*/

    // MARK : Get Current Level Datas for level or childrenLevel
    getCurrentLevelOrChildrens() {
        let selfClass = this;
        let currentDatas = [];

    	if (this.currentChildren !== null) {
            this.currentChildren.forEach(function(key) {
                currentDatas.push(selfClass.levelsData[selfClass.currentLevel].children[key]);
            });
    	} else {
            currentDatas.push(this.levelsData[this.currentLevel]);
    	}

        return currentDatas;
    }

    getSpecificLevelOrChildren(levelID, childrenID) {
        let selfClass = this;
        let currentDatas = [];

    	if (childrenID !== null) {
    		currentDatas.push(selfClass.levelsData[levelID].children[childrenID]);
    	} else {
    		currentDatas.push(selfClass.levelsData[levelID]);
    	}

        return currentDatas;
    }

    setViewedCurrentLevelOrChildren(childrenID) {
        let selfClass = this;

    	if (this.currentChildren !== null) {
            this.levelsData[this.currentLevel].children[childrenID].viewed = true; 

            var childrens = this.getCurrentLevelOrChildrens();
            var isAllViewed = Object.keys(childrens).every(function(key){ return childrens[key].viewed === true });

            if(isAllViewed) {
                return true;
            }

            return false;

    	} else {
    		this.levelsData[this.currentLevel].viewed = true;	
            return true;
    	}
    }

    getNextLevel() {
        var currentObj = this.levelsData[this.currentLevel];
        this.currentChildren = null;

        if (currentObj.viewed === true && currentObj.children !== null) {
            var childrenOne = currentObj.children[1];
            var childrenTwo = currentObj.children[2];

            if(childrenOne.viewed == true && childrenTwo.viewed == true) {
                this.checkNextLevel();
            } else {
                childrenOne.unlock = childrenTwo.unlock = true;
                this.currentChildren = [1, 2];
            }
        } else if(currentObj.viewed !== false) {
            this.checkNextLevel();
        }
    }

    checkNextLevel() {
        var currentObj = this.levelsData[this.currentLevel+1];
                
        if (currentObj !== undefined) {
            this.increaseCurrentLevel();
            this.currentChildren = null;
            currentObj.unlock = true;
        }

        return false;
    }
} 