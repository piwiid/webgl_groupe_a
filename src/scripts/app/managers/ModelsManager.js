/**
*
* ModelsManager.js
*
**/

// Global dependencies
import * as THREE from 'three';
import { Promise } from 'es6-promise';

import {TimelineLite} from 'gsap'

// Loader dependencies;
import MTLLoader from 'three/examples/js/loaders/MTLLoader';
import LoaderSupport from 'three/examples/js/loaders/LoaderSupport';
import OBJLoader from 'three/examples/js/loaders/OBJLoader2';

// Data dependencies
import modelsData from 'datasources/ModelsData';
import { Bus } from "../helpers/events/Bus";

// Helpers dependencies
import Helpers from 'helpers/helpers';

export default class ModelsManager {

    // Add textures when designers finish
    constructor(scene, manager) {

        this.scene = scene;
        this.manager = manager;
        // Add manager to dertemine the load
        //this.loader = new THREE.ColladaLoader(manager);
        this.obj = modelsData.modelsFile;
        this.objectRaycaster = [];
        this.buildingModel = [];
        this.journaliste = null;
        this.persoModel = [];
        this.lengthLoad = this.obj.length;
        this.totalModel = this.obj.length;
        this.loadModel = 0;
        this.Bus = new Bus();
        this.tl = new TimelineLite();



    }

    load() {
        var game = {score:0};
        this.obj.forEach((item, index) => {

            let itemIndex = index+1;
            let model = item.model

            let promise = () => {
                return new Promise((resolve => {



                    let objLoader = new THREE.OBJLoader2(this.manager);

                    let onLoadMtl = ( materials ) => {
                        //this.objLoader.setModelName( modelName );
                        objLoader.setMaterials( materials );
                        objLoader.setLogging( true, true );
                        objLoader.load( item.model, (event) => {


                            event.detail.loaderRootNode.scale.x = event.detail.loaderRootNode.scale.y = event.detail.loaderRootNode.scale.z = item.scale;


                            if (item.type == 'building') {
                                this.organizeBuildingModel(itemIndex, event.detail.loaderRootNode, item);

                                event.detail.loaderRootNode.traverse((child) => {
                                    if (child instanceof THREE.Mesh) {
                                        this.objectRaycaster.push(child);
                                    }
                                });
                            }


                            if (item.type == 'perso') {
                                this.organizePersoModel(itemIndex, event.detail.loaderRootNode);
                            }




                            this.scene.add( event.detail.loaderRootNode );

                            // Model has load
                            this.loadModel++;
                            let percentage = ((this.loadModel / this.totalModel)).toFixed(1) * 100;
                            this.Bus.dispatch(this.Bus.types.PERCENTAGE_OF_LOAD, { percentage: percentage});

                            if(this.loadModel === this.totalModel && percentage === 100) {
                                this.Bus.dispatch(this.Bus.types.ON_MODELS_LOAD, { load: true});
                            }



                            //

                            resolve();

                        }, null, null, null, true );
                    };
                    objLoader.loadMtl( item.material, null, onLoadMtl );




                }))
            };

            promise();

        });
    }

    organizeBuildingModel(index, model, item) {
        let childrenName = "";
        let isActive = false;

        if (index !== null) {
            childrenName = "building-level" + index;
            isActive = (index === 1) ? true : false;
        }

        switch(index) {
            case 1:
                model.position.set(200, 0, 200);
                break;
            case 2:
                model.position.set(-200, 500, 200);
                break;
            case 3:
                model.position.set(200, 500, -200);
                break;
            case 4:
                model.position.set(-200, 500, -200);
                break;
            case 5:
                model.position.set(0, 1000, 0);
                break;
            default:
                model.position.set(1000, 1000, 1000);
        }

        model.name = childrenName;
        model.levelIndex = index;
        model.visible = isActive;
        model.clickable = isActive;

        for(let i = 0; model.children.length > i; i++) {
            let element = model.children[i];
            element.nameOld = element.name;
            element.name = childrenName;
            
            //console.log(element.nameOld);

            if(element.nameOld !== item.modelData.startActiveMesh) {
                element.position.set(element.position.x, 500, element.position.y); 
                //element.material.opacity = 0;
                element.visible = false;
            }
        }


        this.buildingModel.push(model);
        return model;
    }

    organizePersoModel(index, model) {
        let childrenName = "";

        if (index !== null) {
            childrenName = "perso-" + index;
        }

        switch(index) {
            case 6:
                this.journaliste = model;
                model.position.set(200, 72, 200);
                break;
            default:
                model.position.set(1000, 1000, 1000);
        }

        model.name = childrenName;

        for(let i = 0; model.children.length > i; i++) {
            let element = model.children[i];
            element.nameOld = element.name;
            element.name = childrenName;
        }


        this.persoModel.push(model);
        return model;
    }
}
