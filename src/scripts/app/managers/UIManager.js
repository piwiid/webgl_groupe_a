/**
*
* UIManager.js
*
**/

// Styles dependencies
import 'ressources/styles/index.scss';

// Main class dependencies
import Main from 'main';

// Logo & Socials Images
import logoGobelins from 'ressources/images/logo_gobelins.png';
import logoCCI from 'ressources/images/logo_cci.png';
import logoSocials from 'ressources/images/social_icons.png';

// Move Images
import moveScroll from 'ressources/images/move_scroll.png';
import moveDrag from 'ressources/images/move_drag.png';
import pinInstruction from 'ressources/images/pin_info.png';


// LaunchScreen
let launchScreen;

// Utils variables
let mainContainer;
let titleScreen, introContainer;
let startButton, finishButton;
let appContainer, helpButton, helpModal, closeHelpModal;

// FinishScreen Components
let finishScreen;
let restartButton, sourceButton;
let sourceModal, closeSourcesModal;


export default class UIManager {

    // MARK : Custom main methods
    setupTitleScreen() {
        
        this.setupImages();

        launchScreen = document.getElementById('launchContainer');
        //launchScreen.style.display = 'none';

        titleScreen = document.getElementById('titleContainer');
        //titleScreen.style.display = 'none';
        //this.fadeOut(titleScreen, 2000);

        //this.animateLaunchScreen();

        introContainer = document.getElementById('introContainer');
        //introContainer.style.display = 'none';

        //this.fadeIn(introContainer, 3000);

        finishScreen = document.getElementById('finishContainer');
        //finishScreen.style.display = 'none';

        startButton = document.getElementById('startButton');
        startButton.disabled = true;
        startButton.addEventListener('click', () => {
            this.hideStartUI();
        });

        helpButton = document.getElementById('help-icon');
        helpButton.addEventListener('click', () => {
            this.showAppInstructions();
        });

        closeHelpModal = document.getElementById('closeHelpModal');
        closeHelpModal.addEventListener('click', () => {
            this.hideAppInstructions();
        });

        finishButton = document.getElementById('finishButton');
        finishButton.addEventListener('click', () => {
            this.showFinishScreen();
        });


        // Finish Screen buttons
        restartButton = document.getElementById('restartButton');
        restartButton.addEventListener('click', () => {
            //this.resetApp();
            this.hideStartUI();
        });

        sourceButton = document.getElementById('sourcesButton');
        sourceButton.addEventListener('click', () => {
            this.showSourcesModal();
        });

        closeSourcesModal = document.getElementById('closeSourcesModal');
        closeSourcesModal.addEventListener('click', () => {
            this.hideSourcesModal();
        });


    }

    setupImages() {
        const img_gobelins = document.getElementById('logoGobelins');
        img_gobelins.src = logoGobelins;

        const img_cci = document.getElementById('logoCCI');
        img_cci.src = logoCCI;

        const img_scroll = document.getElementsByClassName('moveScroll');
        for(var i=0; i < img_scroll.length; i++) { 
            img_scroll[i].src = moveScroll;
        }

        const img_pinInstruction = document.getElementsByClassName('pinInstruction');
        for(var i=0; i < img_pinInstruction.length; i++) { 
            img_pinInstruction[i].src = pinInstruction;
        }
        
        const img_drag = document.getElementsByClassName('moveDrag');
        for(var i=0; i < img_scroll.length; i++) { 
            img_drag[i].src = moveDrag;
        }

        const img_socials = document.getElementById('logoSocials');
        img_socials.src = logoSocials;
    }

    setupContainer() {
        const container = document.getElementById('appContainer');
        appContainer = new Main(container);

        startButton.disabled = false;
    }



    displayPanel() {

        // ELEMENTS OF PANEL




    }


    // MARK : SHOW / HIDE UI
    hideStartUI() {
        //TweenLite.fromTo('#mainContainer', 1,{autoAlpha:1}, {autoAlpha:0,display:'none'});
        /*introContainer.style.display = 'none';
        mainContainer = document.getElementById('mainContainer');
        this.fadeOut(mainContainer, 20);*/
    }

    showAppInstructions() {
        helpModal = document.getElementById('help-modal');
        helpModal.style.display = 'flex';
    }

    hideAppInstructions() {
        helpModal = document.getElementById('help-modal');
        helpModal.style.display = 'none';
    }


    // MARK : FinishScreen UI
    showFinishScreen() {
        finishScreen.style.display = 'flex';
        mainContainer = document.getElementById('mainContainer');
        this.fadeIn(mainContainer, 20);
    }

    showSourcesModal() {
        sourceModal = document.getElementById('sources-modal');
        sourceModal.style.display = 'flex';
    }

    hideSourcesModal() {
        sourceModal = document.getElementById('sources-modal');
        sourceModal.style.display = 'none';
    }


    // MARK : SHOW / HIDE UI

    // Helpers Methods
     fadeOut(element, delay) {
        setTimeout(function() {     
            var op = 1;  // initial opacity
            var timer = setInterval(function () {
                if (op <= 0.01){
                    clearInterval(timer);
                    element.style.display = 'none';
                }
                element.style.opacity = op;
                element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                op -= op * 0.1;
            }, 50);
        }, delay); 
    }

     fadeIn(element, delay) {
        setTimeout(function() {     
            var op = 0.01;  // initial opacity
            var timer = setInterval(function () {
                if (op >= 1){
                    clearInterval(timer);
                }
                element.style.opacity = op;
                element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                element.style.display = 'flex';
                op += op * 0.1;
            }, 50);
        }, delay);
    }

}
