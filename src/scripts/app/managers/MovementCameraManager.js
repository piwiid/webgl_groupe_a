/**
*
* MovementCameraManager.js
*
**/

// Global dependencies
import * as THREE from "three";
import TWEEN from 'tween.js'


export function tweenRotation(target, object, orbit, camera, cameraLookAt){


    var controls = orbit;

    var axis = new THREE.Vector3( 0, 1, 0 );

    TWEEN.removeAll();

    var vectorCamera = new THREE.Vector3(0,0,0).copy(camera.threeCamera.position);

    cameraLookAt = new THREE.Vector3(object.parent.parent.position.x, object.parent.parent.position.y + 100, object.parent.parent.position.z);
    controls.target.copy(cameraLookAt);

    new TWEEN.Tween(vectorCamera).to(target, 2000)
        .easing( TWEEN.Easing.Quadratic.InOut )
        .onUpdate(() => {

            camera.threeCamera.position.set(vectorCamera.x, vectorCamera.y, vectorCamera.z)

        })
        .onComplete(() => {
            //this.click = true;
            //this.updateCamera();



            controls.enabled = true;

            new TWEEN.Tween(vectorCamera).to(new THREE.Vector3(target.x, target.y - 100, target.z), 1000)
                .easing( TWEEN.Easing.Quadratic.InOut )
                .onUpdate(() => {

                    axis = object.position;

                    camera.threeCamera.position.set(vectorCamera.x, vectorCamera.y, vectorCamera.z)

                })
                .onComplete(() => {


                })
                .start();
        })
        .start();

}

/**
 * Mouvement de la camera sur un objet
 *
 getCameraLookAt() {
        let scale = 0.001;
        let point = Math.floor(this.splinePoints.length * scrollPosition + this.splinePoints.length) % this.splinePoints.length;

        for(let lookAtObject of this.lookAtObjects) {
            if(point >= lookAtObject.begin && point <= lookAtObject.end) {
                return new THREE.Vector3(lookAtObject.object.position.x * scale, lookAtObject.object.position.y * scale, lookAtObject.object.position.z * scale)
            }
        }
        let pointNext = (point + 1) % this.splinePoints.length;
        return this.initialSplinePosition[pointNext + 1]
    }

 * Mouvement de la camera sur un objet
 * updateCameraPosition() {
        console.log(scrollPosition);
        if(scrollPosition >= 0.99) {
            cancelAnimationFrame(animation);
            scrollPosition = 0;
        } else {
            scrollPosition += 0.008;
            let t = this.getCameraPosition(),
                e = this.getCameraLookAt();

            this.camera.threeCamera.position.set(t.x, t.y, t.z);

            if(this.lookAtObjects[14].position) {
                this.camera.threeCamera.lookAt(this.lookAtObjects[14].position);
            }

            animation = requestAnimationFrame(this.updateCameraPosition.bind(this));
        }

    }**/
