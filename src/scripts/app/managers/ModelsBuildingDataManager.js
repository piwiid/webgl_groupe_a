/**
*
* ModelsBuildingDataManager.js
*
**/

// Global dependencies
import * as THREE from 'three';

// Datas dependencies
import modelsBuildingData from "datasources/modelsBuildingData";


export default class ModelsBuildingDataManager {
	constructor(models) {
		this.models = models;
		this.modelsBuildingData = modelsBuildingData;
    }


    getMeshCount(level) {
    	return this.modelsBuildingData[level].buildingMeshs.length;
    }	

    getBuildingData(level) {
    	return this.models.buildingModel.find(item => item.levelIndex == level);
    }

    getElementToDisplayed(buildingToConstruct, level) {
    	let nextMeshToDisplay = this.modelsBuildingData[level].buildingMeshs.find(item => item.active == false);
        
        if (nextMeshToDisplay !== undefined) {
    	   return buildingToConstruct.children.find(item => item.nameOld == nextMeshToDisplay.name);
        } else {
            return null;
        }
    }

    setElementToDisplayed(buildingToConstruct, level) {
    	let nextMeshDisplayed = this.modelsBuildingData[level].buildingMeshs.find(item => item.active == false);
    	nextMeshDisplayed.active = true;
    }

}